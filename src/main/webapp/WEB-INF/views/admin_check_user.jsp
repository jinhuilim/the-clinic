<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="s" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Patient User List</title>
</head>
<body>
<h3>Patient information:</h3>
 <table border="1">
                        <tr>
                            <th>id</th>
                            <th>username</th>
                            <th>gender</th>
                            <th>age</th>
                            <th>email</th>
                        </tr>
                            <tr>
                                <td>${user_list.id}</td>
                                <td>${user_list.username}</td>
                                <td>${user_list.gender}</td>
                                <td>${user_list.age}</td>
                                <td>${user_list.email}</td>
                                <td><a href="/clinic/admin/delete_user/${user_list.id}">Delete</a></td>                                
                            </tr>
                    </table>
<br>
<br>
<h3>Prescription:</h3>
<table border="1">
                        <tr>
                       		<th>Serial Number</th>
                            <th>id</th>
                            <th>Patient Name</th>
                            <th>Medicine Name</th>
                            <th>Doctor Name</th>
                        </tr>
                        <c:forEach var="u" items="${user_prescription}" varStatus="st">
                            <tr>
                            	<td>${st.count}</td>
                                <td>${u.id}</td>
                                <td>${u.patient_name}</td>
                                <td>${u.medicine_name}</td>
                                <td>${u.doctor_name}</td>
                                <td><a href="/clinic/admin/delete_prescription/${u.id}">Delete</a></td>
                                <td><a href="edit_prescription/${u.id}">Edit</a></td>
                            </tr>
                        </c:forEach>
                    </table>
                  <br>
<br>
<h3>Appointment:</h3>
<table border="1">
                        <tr>
                       		<th>Serial Number</th>
                            <th>id</th>
                            <th>Patient Name</th>
                            <th>Date</th>
                            <th>Doctor Name</th>
                        </tr>
                        <c:forEach var="u" items="${user_appointment}" varStatus="st">
                            <tr>
                            	<td>${st.count}</td>
                                <td>${u.id}</td>
                                <td>${u.patient_name}</td>
                                <td>${u.date}</td>
                                <td>${u.doctor_name}</td>
                                <td><a href="/clinic/admin/delete_appointment/${u.id}">Delete</a></td> 
                                <td><a href="edit_appointment/${u.id}">Edit</a></td>
                            </tr>
                        </c:forEach>
                    </table>
                      <br>
<br>
<h3>Messages:</h3>
<table border="1">
                        <tr>
                       		<th>Serial Number</th>
                            <th>id</th>
                            <th>Name</th>
                            <th>Message</th>
                        </tr>
                        <c:forEach var="u" items="${user_review}" varStatus="st">
                            <tr>
                            	<td>${st.count}</td>
                                <td>${u.id}</td>
                                <td>${u.name}</td>
                                <td>${u.messages}</td>
                                <td><a href="/clinic/admin/delete_review/${u.id}">Delete</a></td>                      
                            </tr>
                        </c:forEach>
                    </table>
</body>
</html>