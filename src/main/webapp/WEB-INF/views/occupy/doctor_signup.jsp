<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>Login</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="icon" type="image/png" href="../static/images/icons/favicon.ico"/>

	<link rel="stylesheet" type="text/css" href="../static/vendor/bootstrap/css/bootstrap.min.css">

	<link rel="stylesheet" type="text/css" href="../static/fonts/font-awesome-4.7.0/css/font-awesome.min.css">

	<link rel="stylesheet" type="text/css" href="../static/fonts/Linearicons-Free-v1.0.0/icon-font.min.css">

	<link rel="stylesheet" type="text/css" href="../static/vendor/animate/animate.css">

	<link rel="stylesheet" type="text/css" href="../static/vendor/css-hamburgers/hamburgers.min.css">

	<link rel="stylesheet" type="text/css" href="../static/vendor/animsition/css/animsition.min.css">

	<link rel="stylesheet" type="text/css" href="../static/vendor/select2/select2.min.css">

	<link rel="stylesheet" type="text/css" href="../static/vendor/daterangepicker/daterangepicker.css">

	<link rel="stylesheet" type="text/css" href="../static/css/util.css">
	<link rel="stylesheet" type="text/css" href="../static/css/main.css">

</head>
<body>
	
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Register Page</title>
<script language="javascript">
   var flag=0;//全局变量
   function onChang1(obj){
	   var obValue=obj.value;
	   if(obValue.length>8||obValue.length<3){
		  document.getElementById("usName").innerHTML="<font name='usName' style='font-size:12px;color:red'>Length should be 3-8 characters!</font>";
           flag=0;
	   }else{
			document.getElementById("usName").innerHTML="<font name='usName' style='font-size:12px;color:green'>Available</font>";
            flag++;
	   }
   }
  function onChang2(obj){
	  var obValue=obj.value;
	   if(obValue.length>8||obValue.length<6){
		  document.getElementById("usPass").innerHTML="<font name='usPass' style='font-size:12px;color:red'>Length should be 6-8 characters!</font>";
          flag=0;
		 
	   }else{
			document.getElementById("usPass").innerHTML="<font name='usPass' style='font-size:12px;color:green'>Available</font>";
           flag++;
	   } 
   }
  function onChang3(obj){
	  var obValue=obj.value;
	  var pass=document.getElementById("userpass").value;
      if(obValue!=pass){
		 document.getElementById("usPass1").innerHTML="<font name='usPass1' style='font-size:12px;color:red'>Please confirm your password!</font>";
         flag=0;
    	  
      }else{
    	  
 		 document.getElementById("usPass1").innerHTML="<font name='usPass1' style='font-size:12px;color:green'>Pass</font>";
          flag++;
      }
  }
  function res(){
	  document.getElementById("username").value="";
	  document.getElementById("userpass").value="";
	  document.getElementById("userpass1").value="";
	  document.getElementById("userage").value="";
	  document.getElementById("useremail").value="";
	  document.getElementById("gender").value="";
	  document.getElementById("department").value="";
	  
  }
  function sub(){
	var name=document.getElementById("username").value;
	var pass=document.getElementById("userpass").value;
	var pass1=document.getElementById("userpass1").value;
	if(name!=null&&pass!=null&&pass1!=null&&pass.equals("pass1")){
		if(flag==3){
			document.register.submit();
		}else{
			alert("Please enter your information seriously!");
			document.getElementById("username").value="";
			document.getElementById("userpass").value="";
			document.getElementById("userpass1").value="";
			document.getElementById("usName").innerHTML="";
			document.getElementById("usPass").innerHTML="";
			document.getElementById("usPass1").innerHTML="";
			document.getElementById("department").innerHTML="";
		}
	}
  }
</script>
</head>
<body>
<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100 p-l-55 p-r-55 p-t-65 p-b-50">
   <form  method="post" action="doctorRegisterDo" name="register">
      <table style="height:100%;width:100%">
         <tr align="center" valign="middle">
            <td>
                <table width="392" height="200" border=0 align="center">
                   <tbody>
                     <tr valign="middle" align="center">
                        <td colspan="3" height="40" valign="middle" align="center">
                           <font face="黑体" size="5px" color="#196ed1" style="padding-left:20px;">Doctor Register !  </font>
                        </td>
                     </tr>
                     <tr>
                        <td>Username：</td>
                        <td><input type="text" name="doctor_name" id="username" value="" onChange="onChang1(this)">
                        <td id="usName"></td>
                     </tr>
                     
                      <tr>
                        <td>Password：</td>
                        <td><input type="password" name="doctor_password" id="userpass" value="" onChange="onChang2(this)">
                        <td id="usPass"></td>
                     </tr>
                     <tr>
                        <td>Confirm Password：</td>
                        <td><input type="password" name="password2" id="userpass1" value="" onChange="onChang3(this)">
                        <td id="usPass1"></td>
                     </tr>
                     <tr>
                        <td>Gender：</td>
                        <td><select id="gender" name="doctor_gender">
                        		<option value="male">Male</option>
                        		<option value="female">Female</option>
                        </select>
                        <td id="gender1"></td>
                     </tr>
                     
                     <tr>
                        <td>Age：</td>
                        <td><input type="text" name="doctor_age"  id="userage">
                        <td id="age1"></td>
                     </tr>
                     
                     <tr>
                        <td>Email：</td>
                        <td><input type="text" name="doctor_email" id="useremail">
                        <td id="email1"></td>
                     </tr>
                     <tr>
                        <td>Department：</td>
                        <td><select id="department" name="department">
                        		<option value="Surgery">Surgery</option>
                        		<option value="Cardiology">Cardiology</option>
                        		<option value="Neurology">Neurology</option>
                        		<option value="Physiotherapy">Physiotherapy</option>
                        		<option value="Microbiology">Microbiology</option>
                        		<option value="Dentistry">Dentistry</option>
                        </select>
                        <td id="department1"></td>
                     </tr>
                     <tr>
                       <td><input type="submit" value="Submit" onClick="sub()">
                       <td><input type="button" value="Reset" onClick="res()">
                     </tr>
                   </tbody>
               </table>
         </tr>
      </table>
    </form>
    			</div>
		</div>
	</div>
	

	
<!--===============================================================================================-->
	<script src="../static/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="../static/vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="../static/vendor/bootstrap/js/popper.js"></script>
	<script src="../static/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="../static/vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="../static/vendor/daterangepicker/moment.min.js"></script>
	<script src="../static/vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="../static/vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="../static/js/main.js"></script>

</body>
</html>