<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>Login</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="icon" type="image/png" href="../static/images/icons/favicon.ico"/>

	<link rel="stylesheet" type="text/css" href="../static/vendor/bootstrap/css/bootstrap.min.css">

	<link rel="stylesheet" type="text/css" href="../static/fonts/font-awesome-4.7.0/css/font-awesome.min.css">

	<link rel="stylesheet" type="text/css" href="../static/fonts/Linearicons-Free-v1.0.0/icon-font.min.css">

	<link rel="stylesheet" type="text/css" href="../static/vendor/animate/animate.css">

	<link rel="stylesheet" type="text/css" href="../static/vendor/css-hamburgers/hamburgers.min.css">

	<link rel="stylesheet" type="text/css" href="../static/vendor/animsition/css/animsition.min.css">

	<link rel="stylesheet" type="text/css" href="../static/vendor/select2/select2.min.css">

	<link rel="stylesheet" type="text/css" href="../static/vendor/daterangepicker/daterangepicker.css">

	<link rel="stylesheet" type="text/css" href="../static/css/util.css">
	<link rel="stylesheet" type="text/css" href="../static/css/main.css">

</head>
<body>
	
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100 p-l-55 p-r-55 p-t-65 p-b-50">
				<form action="login" method="post">
        <table>
       	 	
            <tr>
                <td>Username:</td>
                <td><input type="text" name="username" /></td>
            </tr>
            <tr>
                <td>Password:</td>
                <td><input type="password" name="password" /></td>
            </tr>
            <tr>
                <td><input type="submit" value="Login" /></td>
                <td></td>
            </tr>
        </table>
    </form>

					<div class="text-center p-t-45 p-b-4">
						<span class="txt1">
							Are you a doctor?
						</span>
						<a href="<c:url value="/doctor/accessDoctorLogin"/>">Doctor Access</a>
					</div>

					<div class="text-center">
						<span class="txt1">
							Create an account?
						</span>

						<a href="Signup.html" class="txt2 hov1">
							Sign up
						</a>
					</div>
				
			</div>
		</div>
	</div>
	

	
<!--===============================================================================================-->
	<script src="../static/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="../static/vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="../static/vendor/bootstrap/js/popper.js"></script>
	<script src="../static/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="../static/vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="../static/vendor/daterangepicker/moment.min.js"></script>
	<script src="../static/vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="../static/vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="../static/js/main.js"></script>

</body>
</html>