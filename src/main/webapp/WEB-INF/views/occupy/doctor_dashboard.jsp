<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="ISO-8859-1">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="icon" href="../static/img/favicon.png" type="image/png">
        <title>Dashboard</title>
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="../static/css/bootstrap.css">
        <link rel="stylesheet" href="../static/vendors/linericon/style.css">
        <link rel="stylesheet" href="../static/css/font-awesome.min.css">
        <link rel="stylesheet" href="../static/vendors/owl-carousel/owl.carousel.min.css">
        <link rel="stylesheet" href="../static/vendors/lightbox/simpleLightbox.css">
        <link rel="stylesheet" href="../static/vendors/nice-select/css/nice-select.css">
        <link rel="stylesheet" href="../static/vendors/animate-css/animate.css">
        <link rel="stylesheet" href="../static/vendors/swiper/css/swiper.min.css">
        <!-- main css -->
        <link rel="stylesheet" href="../static/css/style.css">
        <link rel="stylesheet" href="../static/css/responsive.css">
    </head>
    <body>
        
        <!--================Header Menu Area =================-->
        <header class="header_area">
            <div class="main_menu">
            	<nav class="navbar navbar-expand-lg navbar-light">
					<div class="container box_1620">
						<!-- Brand and toggle get grouped for better mobile display -->
						<a class="navbar-brand logo_h" href="index.html"><img src="../static/img/logo.png" alt=""></a>
						<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<!-- Collect the nav links, forms, and other content for toggling -->
						<div class="collapse navbar-collapse offset" id="navbarSupportedContent">
							<ul class="nav navbar-nav menu_nav justify-content-center">
                                <ul class="nav navbar-nav menu_nav justify-content-center">
                                    <li class="nav-item "><a class="nav-link" href="../">Home</a></li>
                                    <li class="nav-item active"><a class="nav-link" href="doctorInfoPage">Dashboard</a>
                                    <li class="nav-item"><a class="nav-link" href="AppointmentList">Appointment</a>
                                    <li class="nav-item"><a class="nav-link" href="prescriptionList">Prescription</a>
                                    <li class="nav-item"><a class="nav-link" href="logout">logout</a>
							</ul>
						</div> 
					</div>
            	</nav>
            </div>
        </header>
        <!--================Header Menu Area =================-->
        
        <!--================Home Banner Area =================-->
        <section class="banner_area">
            <div class="banner_inner d-flex align-items-center">
				<div class="container">
					<div class="banner_content">
						<h2>Dashboard</h2>
						<div class="page_link">
							<a href="index.html">Home</a>
							<h5>Welcome ${sessionScope.doctor.doctor_name}</h5>
						</div>
					</div>
				</div>
            </div>
        </section>
        <!--================End Home Banner Area =================-->
        
        <!--================Success Area =================-->
        <section class="success_area">
        	<div class="row m0">
        		<div class="col-lg-6 p0">
        			<div class="mission_text">
        			<h5>Please check your profile from the following table:</h5>
						 <table border="1">
                        <tr>
                            <th>Name</th>
                            <th>gender</th>
                            <th>age</th>
                            <th>email</th>
                            <th>department</th>
                        </tr>
                            <tr>
                                <td>${profile_list.doctor_name}</td>
                                <td>${profile_list.doctor_gender}</td>
                                <td>${profile_list.doctor_age}</td>
                                <td>${profile_list.doctor_email}</td>  
                                <td>${profile_list.department}</td>                           
                            </tr>
                    </table>
                    <a href="/clinic/doctor/edit_doctor_profile/${profile_list.doctor_id}">Edit my profile</a>
					</div>
        		</div>
        		<div class="col-lg-6 p0">
        			<div class="success_img">
        				<img src="../static/img/success-1.jpg" alt="">
        			</div>
        		</div>
        	</div>
        	<div class="row m0 right_dir">
        		<div class="col-lg-6 p0">
        			<div class="success_img">
        				<img src="../static/img/success-2.jpg" alt="">
        			</div>
        		</div>
        		<div class="col-lg-6 p0">
        			<div class="mission_text">
						<a class="banner_btn" href="AppointmentList">Check Appointment List</a>
						<br>
						<br>
						<a class="banner_btn" href="prescriptionList">Check Prescription List</a>
						<br>
						<br>
						<a class="banner_btn" href="patientList">Check current patients</a>
						<br>
						<br>
						<a class="banner_btn" href="../review/addReviewPage">Send a message to the admin</a>
						<p>
                        </p>
					</div>
        		</div>
        		
        	</div>
        </section>
        <!--================End Success Area =================-->
        
        <!--================ start footer Area  =================-->	
        <footer class="footer-area">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3  col-md-6 col-sm-6">
                        <div class="single-footer-widget">
                            <h6 class="footer_title">About Our Clinic</h6>
                            <p>Since opening in 2018, The Clinic has been one of the most famous clinic in USYD, known for its ability to achieve exceptional results. The Clinic provides the very best services and technologies for your health.</p>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="single-footer-widget">
                            <h6 class="footer_title">Navigation Links</h6>
                            <div class="row">
                                <div class="col-4">
                                    <ul class="list">
                                        <li><a href="#">Home</a></li>
                                        <li><a href="#">About</a></li>
                                        <li><a href="#">Dashboard</a></li>
                                        <li><a href="#">Appointment</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="single-footer-widget">
                            <h6 class="footer_title">Cooperation</h6>
                            <p>Please contact us for business cooperation, </p>
                            <div id="mc_embed_signup">
                                <form target="_blank" action="https://spondonit.us12.list-manage.com/subscribe/post?u=1462626880ade1ac87bd9c93a&amp;id=92a4423d01" method="get" class="subscribe_form relative">
                                    <div class="input-group d-flex flex-row">
                                        <input name="EMAIL" placeholder="Email Address" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Email Address '" required="" type="email">
                                        <button class="btn sub-btn"><span class="lnr lnr-location"></span></button>
                                    </div>
                                    <div class="mt-10 info"></div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="single-footer-widget instafeed">
                            <h6 class="footer_title">InstaFeed</h6>
                            <ul class="list instafeed d-flex flex-wrap">
                                <li><img src="../static/img/instagram/Image-01.jpg" alt=""></li>
                                <li><img src="../static/img/instagram/Image-02.jpg" alt=""></li>
                                <li><img src="../tatic/img/instagram/Image-03.jpg" alt=""></li>
                                <li><img src="../static/img/instagram/Image-04.jpg" alt=""></li>
                                <li><img src="../static/img/instagram/Image-05.jpg" alt=""></li>
                                <li><img src="../static/img/instagram/Image-06.jpg" alt=""></li>
                                <li><img src="../static/img/instagram/Image-07.jpg" alt=""></li>
                                <li><img src="../static/img/instagram/Image-08.jpg" alt=""></li>
                            </ul>
                        </div>
                    </div>
                </div>

            </div>
            <div class="border_line"></div>
            <div class="container">
                <div class="row footer-bottom d-flex justify-content-between align-items-center">
                    <p class="col-lg-8 col-md-8 footer-text m-0">Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved</a></p>

                </div>
            </div>
        </footer>
        <!--================ End footer Area  =================-->





        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="../static/js/jquery-3.2.1.min.js"></script>
        <script src="../static/js/popper.js"></script>
        <script src="../static/js/bootstrap.min.js"></script>
        <script src="../static/js/stellar.js"></script>
        <script src="../static/vendors/lightbox/simpleLightbox.min.js"></script>
        <script src="../static/vendors/nice-select/js/jquery.nice-select.min.js"></script>
        <script src="../static/vendors/isotope/imagesloaded.pkgd.min.js"></script>
        <script src="../static/vendors/isotope/isotope-min.js"></script>
        <script src="../static/vendors/owl-carousel/owl.carousel.min.js"></script>
        <script src="../static/js/jquery.ajaxchimp.min.js"></script>
        <script src="../static/vendors/counter-up/jquery.waypoints.min.js"></script>
        <script src="../static/vendors/counter-up/jquery.counterup.js"></script>
        <script src="../static/js/mail-script.js"></script>
        <script src="../static/vendors/popup/jquery.magnific-popup.min.js"></script>
        <script src="../static/vendors/swiper/js/swiper.min.js"></script>
        <script src="../static/js/theme.js"></script>
    </body>
</html>