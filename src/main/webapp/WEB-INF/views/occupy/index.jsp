<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="icon" href="img/favicon.png" type="image/png">
        <title>Online Clinic System</title>
        <link rel="stylesheet" href="static/css/bootstrap.css">
        <link rel="stylesheet" href="static/vendors/linericon/style.css">
        <link rel="stylesheet" href="static/css/font-awesome.min.css">
        <link rel="stylesheet" href="static/vendors/owl-carousel/owl.carousel.min.css">
        <link rel="stylesheet" href="static/vendors/lightbox/simpleLightbox.css">
        <link rel="stylesheet" href="static/vendors/nice-select/css/nice-select.css">
        <link rel="stylesheet" href="static/vendors/animate-css/animate.css">
        <link rel="stylesheet" href="static/vendors/swiper/css/swiper.min.css">

        <link rel="stylesheet" href="static/css/style.css">
        <link rel="stylesheet" href="static/css/responsive.css">
    </head>
    <body>
        
        <!--================Header Menu Area =================-->
        <header class="header_area">
            <div class="main_menu">
            	<nav class="navbar navbar-expand-lg navbar-light">
					<div class="container box_1620">
						<!-- Brand and toggle get grouped for better mobile display -->
						<a class="navbar-brand logo_h" href="static/index.html"><img src="static/img/logo.png" alt=""></a>
						<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<!-- Collect the nav links, forms, and other content for toggling -->
						<div class="collapse navbar-collapse offset" id="navbarSupportedContent">
							<ul class="nav navbar-nav menu_nav justify-content-center">
								<li class="nav-item active"><a class="nav-link" href="index.html">Home</a></li>
								<li class="nav-item"><a class="nav-link" href="about-us.html">About</a></li>
								<li class="nav-item"><a class="nav-link" href="dashboard.html">Dashboard</a>
								<li class="nav-item"><a class="nav-link" href="Appointment.html">Appointment</a>
								<li class="nav-item"><a class="nav-link" href="contact.html">Contact</a></li>
							</ul>
						</div>
					</div>
            	</nav>
            </div>
        </header>
        <!--================Header Menu Area =================-->
        
        <!--================Home Banner Area =================-->
        <section class="home_banner_area">
            <div class="swiper-container">
				<div class="swiper-wrapper">
					<div class="swiper-slide"><img src="static/img/slider/slider-1.jpg" alt="">
						<div class="slider_text_inner">
							<div class="container">
								<div class="row">
									<div class="col-lg-7">
										<div class="slider_text">
											<h2>What we care about<br />is your health</h2>
											<p>The time on the server is ${serverTime}.</p>
											<a class="banner_btn" href=user/accessloginpage>Log In</a>
											<a class="banner_btn2" href=user/register>Sign Up</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
        </section>
        <!--================End Home Banner Area =================-->

        
        <!--================ start footer Area  =================-->	
        <footer class="footer-area">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3  col-md-6 col-sm-6">
                        <div class="single-footer-widget">
                            <h6 class="footer_title">About Our Clinic</h6>
                            <p> Since opening in 2018, The Clinic has been one of the most famous clinic in USYD, known for its ability to achieve exceptional results. The Clinic provides the very best services and technologies for your health.</p>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="single-footer-widget">
                            <h6 class="footer_title">Navigation Links</h6>
                            <div class="row">
                                <div class="col-4">
                                    <ul class="list">
                                        <li><a href="#">Home</a></li>
                                        <li><a href="#">About</a></li>
                                        <li><a href="#">Dashboard</a></li>
                                        <li><a href="#">Appointment</a></li>
                                    </ul>
                                </div>										
                            </div>							
                        </div>
                    </div>							
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="single-footer-widget">
                            <h6 class="footer_title">Cooperation</h6>
                            <p>Please contact us for business cooperation, </p>
                            <div id="mc_embed_signup">
                                <form target="_blank" action="https://spondonit.us12.list-manage.com/subscribe/post?u=1462626880ade1ac87bd9c93a&amp;id=92a4423d01" method="get" class="subscribe_form relative">
                                    <div class="input-group d-flex flex-row">
                                        <input name="EMAIL" placeholder="Email Address" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Email Address '" required="" type="email">
                                        <button class="btn sub-btn"><span class="lnr lnr-location"></span></button>		
                                    </div>									
                                    <div class="mt-10 info"></div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="single-footer-widget instafeed">
                            <h6 class="footer_title">Doctors</h6>
                            <ul class="list instafeed d-flex flex-wrap">
                                <li><img src="static/img/instagram/Image-01.jpg" alt=""></li>
                                <li><img src="static/img/instagram/Image-02.jpg" alt=""></li>
                                <li><img src="static/img/instagram/Image-03.jpg" alt=""></li>
                                <li><img src="static/img/instagram/Image-04.jpg" alt=""></li>
                                <li><img src="static/img/instagram/Image-05.jpg" alt=""></li>
                                <li><img src="static/img/instagram/Image-06.jpg" alt=""></li>
                                <li><img src="static/img/instagram/Image-07.jpg" alt=""></li>
                                <li><img src="static/img/instagram/Image-08.jpg" alt=""></li>
                            </ul>
                        </div>
                    </div>
                </div>
                
            </div>
            <div class="border_line"></div>
			<div class="container">
				<div class="row footer-bottom d-flex justify-content-between align-items-center">
                    <p class="col-lg-8 col-md-8 footer-text m-0"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>

                </div>
			</div>
        </footer>
		<!--================ End footer Area  =================-->
        
        
        
        
        
        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="static/js/jquery-3.2.1.min.js"></script>
        <script src="static/js/popper.js"></script>
        <script src="static/js/bootstrap.min.js"></script>
        <script src="static/js/stellar.js"></script>
        <script src="static/vendors/lightbox/simpleLightbox.min.js"></script>
        <script src="static/vendors/nice-select/js/jquery.nice-select.min.js"></script>
        <script src="static/vendors/isotope/imagesloaded.pkgd.min.js"></script>
        <script src="static/vendors/isotope/isotope-min.js"></script>
        <script src="static/vendors/owl-carousel/owl.carousel.min.js"></script>
        <script src="static/js/jquery.ajaxchimp.min.js"></script>
        <script src="static/vendors/counter-up/jquery.waypoints.min.js"></script>
        <script src="static/vendors/counter-up/jquery.counterup.js"></script>
        <script src="static/js/mail-script.js"></script>
        <script src="static/vendors/popup/jquery.magnific-popup.min.js"></script>
        <script src="static/vendors/swiper/js/swiper.min.js"></script>
        <script src="static/js/theme.js"></script>
    </body>
</html>