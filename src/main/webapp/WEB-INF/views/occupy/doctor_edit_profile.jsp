<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    <%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="icon" href="img/favicon.png" type="image/png">
        <title>Occupy Business</title>
        <link rel="stylesheet" href="../../static/css/bootstrap.css">
        <link rel="stylesheet" href="../../static/vendors/linericon/style.css">
        <link rel="stylesheet" href="../../static/css/font-awesome.min.css">
        <link rel="stylesheet" href="../../static/vendors/owl-carousel/owl.carousel.min.css">
        <link rel="stylesheet" href="../../static/vendors/lightbox/simpleLightbox.css">
        <link rel="stylesheet" href="../../static/vendors/nice-select/css/nice-select.css">
        <link rel="stylesheet" href="../../static/vendors/animate-css/animate.css">
        <link rel="stylesheet" href="../../static/vendors/swiper/css/swiper.min.css">
        <!-- main css -->
        <link rel="stylesheet" href="../../static/css/style.css">
        <link rel="stylesheet" href="../../static/css/responsive.css">
    </head>
    <body>
        
        <!--================Header Menu Area =================-->
        <header class="header_area">
            <div class="main_menu">
            	<nav class="navbar navbar-expand-lg navbar-light">
					<div class="container box_1620">
						<!-- Brand and toggle get grouped for better mobile display -->
						<a class="navbar-brand logo_h" href="index.html"><img src="img/logo.png" alt=""></a>
						<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<!-- Collect the nav links, forms, and other content for toggling -->
						<div class="collapse navbar-collapse offset" id="navbarSupportedContent">
							<ul class="nav navbar-nav menu_nav justify-content-center">
						
									<ul class="nav navbar-nav menu_nav justify-content-center">
                                     <li class="nav-item "><a class="nav-link" href="../">Home</a></li>
                                    <li class="nav-item active"><a class="nav-link" href="doctorInfoPage">Dashboard</a>
                                    <li class="nav-item"><a class="nav-link" href="AppointmentList">Appointment</a>
                                    <li class="nav-item"><a class="nav-link" href="prescriptionList">Prescription</a>
                                    <li class="nav-item"><a class="nav-link" href="logout">logout</a>
							</ul>
							</ul>
							<ul class="nav navbar-nav navbar-right">
								<li class="nav-item"><a href="#"><i class="fa fa-facebook"></i></a></li>
								<li class="nav-item"><a href="#"><i class="fa fa-twitter"></i></a></li>
								<li class="nav-item"><a href="#"><i class="fa fa-dribbble"></i></a></li>
								<li class="nav-item"><a href="#"><i class="fa fa-behance"></i></a></li>
								<li class="nav-item"><a href="#" class="search"><i class="lnr lnr-magnifier"></i></a></li>
							</ul>
						</div> 
					</div>
            	</nav>
            </div>
        </header>
        <!--================Header Menu Area =================-->
        
        <!--================Home Banner Area =================-->
        <section class="banner_area">
            <div class="banner_inner d-flex align-items-center">
				<div class="container">
					<div class="banner_content">
						<h2>Appointment List</h2>
					</div>
				</div>
            </div>
        </section>
        <!--================End Home Banner Area =================-->
        
        <!--================Success Area =================-->
        <section class="success_area">
        	<div class="row m0">
        		<div class="col-lg-6 p0">
        			<div class="mission_text">
        			<h5>Customise the profile</h5>
						<sf:form method="POST" methodAttribute="doctor" modelAttribute="doctor">
		<fieldset>
			<table>
				<tr>
					<th><label for="password">password</label></th>
					<td><sf:input path="doctor_password"/></td>
				</tr>
				<tr>
					<th><label for="gender">gender</label></th>
					<td><sf:input path="doctor_gender"/></td>
				</tr>
				<tr>
					<th><label for="age">age</label></th>
					<td><sf:input path="doctor_age"/></td>
				</tr>
				<tr>
					<th><label for="email">email</label></th>
					<td><sf:input path="doctor_email"/></td>
				</tr>
				<tr>
					<th><label for="department">Department</label></th>
					<td><sf:input path="department"/></td>
				</tr>
				<tr>
					<th><a href="../doctorInfoPage"><button>Cancel</button></a></th>		
					<!-- This hidden field is required for Hibernate to recognize this Product -->
					<td><sf:hidden path="doctor_id"/>
					<td><sf:hidden path="doctor_name"/>
					<td><input type="submit" value="Done"/></td>
				</tr>
			</table>
		</fieldset>
	
	</sf:form>
                   <a href="<c:url value="/user/UserInfoPage"/>">Go Back</a><br>
					</div>
        		</div>
        		<div class="col-lg-6 p0">
        			<div class="success_img">
        				<img src="../../static/img/success-1.jpg" alt="">
        			</div>
        		</div>
        	</div>
        	</div>
        </section>
        <!--================End Success Area =================-->
        
        <!--================ start footer Area  =================-->	
        <footer class="footer-area">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="single-footer-widget">
                            <h6 class="footer_title">Navigation Links</h6>
                            <div class="row">
                                <div class="col-4">
                                    <ul class="list">
                                        <li><a href="#">Home</a></li>
                                        <li><a href="#">Feature</a></li>
                                        <li><a href="#">Services</a></li>
                                        <li><a href="#">Portfolio</a></li>
                                    </ul>
                                </div>
                                <div class="col-4">
                                    <ul class="list">
                                        <li><a href="#">Team</a></li>
                                        <li><a href="#">Pricing</a></li>
                                        <li><a href="#">Blog</a></li>
                                        <li><a href="#">Contact</a></li>
                                    </ul>
                                </div>										
                            </div>							
                        </div>
                    </div>							
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="single-footer-widget">
                            <h6 class="footer_title">Newsletter</h6>
                            <p>For business professionals caught between high OEM price and mediocre print and graphic output, </p>		
                            <div id="mc_embed_signup">
                                <form target="_blank" action="https://spondonit.us12.list-manage.com/subscribe/post?u=1462626880ade1ac87bd9c93a&amp;id=92a4423d01" method="get" class="subscribe_form relative">
                                    <div class="input-group d-flex flex-row">
                                        <input name="EMAIL" placeholder="Email Address" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Email Address '" required="" type="email">
                                        <button class="btn sub-btn"><span class="lnr lnr-location"></span></button>		
                                    </div>									
                                    <div class="mt-10 info"></div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="single-footer-widget instafeed">
                            <h6 class="footer_title">InstaFeed</h6>
                            <ul class="list instafeed d-flex flex-wrap">
                                <li><img src="../../static/img/instagram/Image-01.jpg" alt=""></li>
                                <li><img src="../../static/img/instagram/Image-02.jpg" alt=""></li>
                                <li><img src="../../static/img/instagram/Image-03.jpg" alt=""></li>
                                <li><img src="../../static/img/instagram/Image-04.jpg" alt=""></li>
                                <li><img src="../../static/img/instagram/Image-05.jpg" alt=""></li>
                                <li><img src="../../static/img/instagram/Image-06.jpg" alt=""></li>
                                <li><img src="../../static/img/instagram/Image-07.jpg" alt=""></li>
                                <li><img src="../../static/img/instagram/Image-08.jpg" alt=""></li>
                            </ul>
                        </div>
                    </div>						
                </div>
                
            </div>
            <div class="border_line"></div>
			<div class="container">
				<div class="row footer-bottom d-flex justify-content-between align-items-center">
                    <p class="col-lg-8 col-md-8 footer-text m-0">Copyright Â© 2018  |  All rights reserved to <a href="#">Colorlib</a<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
                    <div class="col-lg-4 col-md-4 footer-social">
                        <a href="#"><i class="fa fa-facebook"></i></a>
                        <a href="#"><i class="fa fa-twitter"></i></a>
                        <a href="#"><i class="fa fa-dribbble"></i></a>
                        <a href="#"><i class="fa fa-behance"></i></a>
                    </div>
                </div>
			</div>
        </footer>
		<!--================ End footer Area  =================-->
        
        
        
        
        
        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="../../static/js/jquery-3.2.1.min.js"></script>
        <script src="../../static/js/popper.js"></script>
        <script src="../../static/js/bootstrap.min.js"></script>
        <script src="../../static/js/stellar.js"></script>
        <script src="../../static/vendors/lightbox/simpleLightbox.min.js"></script>
        <script src="../../static/vendors/nice-select/js/jquery.nice-select.min.js"></script>
        <script src="../../static/vendors/isotope/imagesloaded.pkgd.min.js"></script>
        <script src="../../static/vendors/isotope/isotope-min.js"></script>
        <script src="../../static/vendors/owl-carousel/owl.carousel.min.js"></script>
        <script src="../../static/js/jquery.ajaxchimp.min.js"></script>
        <script src="../../static/vendors/counter-up/jquery.waypoints.min.js"></script>
        <script src="../../static/vendors/counter-up/jquery.counterup.js"></script>
        <script src="../../static/js/mail-script.js"></script>
        <script src="../../static/vendors/popup/jquery.magnific-popup.min.js"></script>
        <script src="../../static/vendors/swiper/js/swiper.min.js"></script>
        <script src="../../static/js/theme.js"></script>
    </body>
</html>