<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="s" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<h3>All the Prescription:</h3>
 <table border="1">
                        <tr>
                       		<th>Serial Number</th>
                            <th>id</th>
                            <th>Patient Name</th>
                            <th>Medicine Name</th>
                            <th>Doctor Name</th>
                        </tr>
                        <c:forEach var="u" items="${prescription_list}" varStatus="st">
                            <tr>
                            	<td>${st.count}</td>
                                <td>${u.id}</td>
                                <td>${u.patient_name}</td>
                                <td>${u.medicine_name}</td>
                                <td>${u.doctor_name}</td>
                                <td><a href="/clinic/admin/delete_prescription/${u.id}">Delete</a></td>
                                <td><a href="edit_prescription/${u.id}">Edit</a></td>
                            </tr>
                        </c:forEach>
                    </table>
</body>
</html>