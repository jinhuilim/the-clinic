<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Welcome back Admin!</title>
</head>
<body>

	<h1>Admin Home Page</h1>
	<a href="<c:url value="/admin/patientlist"/>">view information of patients</a><br>
	<a href="<c:url value="/admin/doctorlist"/>">view information of doctors</a><br>
	<a href="<c:url value="/admin/appointmentlist"/>">view appointment</a><br>
	<a href="<c:url value="/admin/prescriptionlist"/>">view prescription</a><br>
	<a href="<c:url value="/admin/reviewlist"/>">view messages</a><br>
	<form action="search_user" method="post">
        <table>
       	
            <tr>
                <td>Enter the patient use you want to search:</td>
                <td><input type="text" name="username" /></td>
            </tr>
            <tr>
                <td><input type="submit" value="search" /></td>
                <td></td>
            </tr>
        </table>
    </form>
    <br>
    <br>
    <form action="search_doctor" method="post">
        <table>
       	
            <tr>
                <td>Enter the doctor use you want to search:</td>
                <td><input type="text" name="username" /></td>
            </tr>
            <tr>
                <td><input type="submit" value="search" /></td>
                <td></td>
            </tr>
        </table>
    </form>

</body>
</html>