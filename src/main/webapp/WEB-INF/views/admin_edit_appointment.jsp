<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<h1>You can edit the Appointment shown below</h1>
	<sf:form method="POST" methodAttribute="appointment" modelAttribute="appointment">
		<fieldset>
			<table>
				<tr>
					<th><label for="date">Date</label></th>
					<td><sf:input path="date"/></td>
				</tr>
				<tr>
					<th><label for="doctor_name">Doctor</label></th>
					<td><sf:input path="doctor_name"/></td>
				</tr>
				<tr>
					<th><label for="patient_name">Patient Name</label></th>
					<td><sf:input path="patient_name"/></td>
				</tr>
				<tr>
					<th><a href="admin_view_appointments.htm"><button>Cancel</button></a></th>		
					<!-- This hidden field is required for Hibernate to recognize this Product -->
					<td><sf:hidden path="id"/>
					<td><input type="submit" value="Done"/></td>
				</tr>
			</table>
		</fieldset>
	
	</sf:form>
</body>
</html>