<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="s" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Patient User List</title>
</head>
<body>
<h3>All the Patient users:</h3>
 <table border="1">
                        <tr>
                       		<th>Serial Number</th>
                            <th>id</th>
                            <th>username</th>
                            <th>gender</th>
                            <th>age</th>
                            <th>email</th>
                        </tr>
                        <c:forEach var="u" items="${patient_list}" varStatus="st">
                            <tr>
                            	<td>${st.count}</td>
                                <td>${u.id}</td>
                                <td>${u.username}</td>
                                <td>${u.gender}</td>
                                <td>${u.age}</td>
                                <td>${u.email}</td>
                                <td><a href="/clinic/admin/delete_user/${u.id}">Delete</a></td>                                
                            </tr>
                        </c:forEach>
                    </table>
</body>
</html>