<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="s" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Appointment List</title>
</head>
<body>
<h3>All the Appointment:</h3>
 <table border="1">
                        <tr>
                       		<th>Serial Number</th>
                            <th>id</th>
                            <th>Patient Name</th>
                            <th>Date</th>
                            <th>Doctor Name</th>
                        </tr>
                        <c:forEach var="u" items="${appointment_list}" varStatus="st">
                            <tr>
                            	<td>${st.count}</td>
                                <td>${u.id}</td>
                                <td>${u.patient_name}</td>
                                <td>${u.date}</td>
                                <td>${u.doctor_name}</td>
                                <td><a href="/clinic/admin/delete_appointment/${u.id}">Delete</a></td> 
                                <td><a href="edit_appointment/${u.id}">Edit</a></td>
                            </tr>
                        </c:forEach>
                    </table>
</body>
</html>