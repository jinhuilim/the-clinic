package au.usyd.clinic.web;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import au.usyd.clinic.domain.Appointment;
import au.usyd.clinic.domain.Prescription;
import au.usyd.clinic.service.AppointmentService;

@Controller
@RequestMapping(value="/appointment")
public class AppointmentController {
	
	
	@Autowired
	AppointmentService as;
	
	@RequestMapping(value="/addAptPage")
	public String addAptPage() {
		
		return "occupy/user_apply_appointment";
	}
	
	@RequestMapping(value="/addApt",method=RequestMethod.POST)
	public String addApt(HttpServletRequest httpServletRequest) {
		
		Appointment apt = new Appointment();
		
		apt.setPatient_name(httpServletRequest.getParameter("patient_name"));
		apt.setDate(httpServletRequest.getParameter("date"));
		apt.setDoctor_name(httpServletRequest.getParameter("doctor_name"));
		
		as.addAppointment(apt);
		
		String patient_name = apt.getPatient_name();
		String date = apt.getDate();
		String doctor_name = apt.getDoctor_name();
		
		httpServletRequest.setAttribute("pname", patient_name);
		httpServletRequest.setAttribute("d", date);
		httpServletRequest.setAttribute("dname", doctor_name);
		return "occupy/user_success";
	}
	

	
}
