package au.usyd.clinic.web;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import au.usyd.clinic.domain.Prescription;
import au.usyd.clinic.service.PrescriptionService;

@Controller
@RequestMapping(value="/prescription")
public class PrescriptionController {
	
	
	@Autowired
	PrescriptionService ps;
	
	@RequestMapping(value="/addPresPage")
	public String addPresPage() {
		
		return "occupy/doctor_give_prescription";
	}
	
	@RequestMapping(value="/addPres",method=RequestMethod.POST)
	public String addPres(HttpServletRequest httpServletRequest) {
		
		Prescription pres = new Prescription();
		
		pres.setPatient_name(httpServletRequest.getParameter("patient_name"));
		pres.setMedince_name(httpServletRequest.getParameter("medicine_name"));
		pres.setDoctor_name(httpServletRequest.getParameter("doctor_name"));
		
		ps.addPrescription(pres);
		
		String patient_name = pres.getPatient_name();
		String medicine_name = pres.getMedicine_name();
		String doctor_name = pres.getDoctor_name();
		
		httpServletRequest.setAttribute("pname", patient_name);
		httpServletRequest.setAttribute("mname", medicine_name);
		httpServletRequest.setAttribute("dname", doctor_name);
		return "occupy/doctor_success";
	}
	
}
