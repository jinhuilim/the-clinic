package au.usyd.clinic.web;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import au.usyd.clinic.domain.Review;
import au.usyd.clinic.service.ReviewService;

@Controller
@RequestMapping(value="/review")
public class ReviewController {
	
	
	@Autowired
	ReviewService rs;
	
	@RequestMapping(value="/addReviewPage")
	public String addPresPage() {
		
		return "occupy/send_review";
	}
	
	@RequestMapping(value="/addReview",method=RequestMethod.POST)
	public String addReview(HttpServletRequest httpServletRequest) {
		
		Review review = new Review();
		
		review.setName(httpServletRequest.getParameter("name"));
		review.setMessages(httpServletRequest.getParameter("messages"));
		
		rs.addReview(review);
		
		String name = review.getName();
		String messages = review.getMessages();
		
		httpServletRequest.setAttribute("username", name);
		httpServletRequest.setAttribute("messages", messages);
		return "occupy/send_review_success";
	}
	
}
