package au.usyd.clinic.web;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import au.usyd.clinic.domain.Doctor;
import au.usyd.clinic.domain.Prescription;
import au.usyd.clinic.domain.User;
import au.usyd.clinic.service.DoctorService;
import au.usyd.clinic.service.AdminService;
import au.usyd.clinic.service.AppointmentService;
import au.usyd.clinic.service.PrescriptionService;

@Controller
@RequestMapping(value="/doctor")
public class DoctorController {
	
	public static String logged_doctor="";
	
	@Autowired
	DoctorService doctorService;
	@Autowired
	AdminService adminService;
	@Autowired
	PrescriptionService prescriptionService;
	@Autowired
	AppointmentService appointmentService;

	
	@RequestMapping(value="/accessDoctorLogin")
	public String doctorLoginPage() {
		
		return "occupy/doctor_login";
	}
	
	@RequestMapping(value="/doctorLogin")
	public String doctorLogin(HttpServletRequest request) {
	
		String doctor_name = request.getParameter("doctor_name");
		String doctor_password = request.getParameter("doctor_password");
		
		Doctor doctor = doctorService.getDoctorByUsername(doctor_name);
		HttpSession session = request.getSession(true);
	    session.setAttribute("doctor", doctor);	
		if(doctor!=null) {
			if(doctor.getDoctor_name().equals(doctor_name)&&doctor.getDoctor_password().equals(doctor_password)) {
				logged_doctor=doctor_name;
				return "redirect:doctorInfoPage";
			}else {
				return "error";
			}
		}else {
			return "error";
		}
		
	}
	
	@RequestMapping(value="/doctorRegister")
	public String doctorRegisterPage() {
		return "occupy/doctor_signup";
	}
	
	@RequestMapping(value="doctorRegisterDo",method=RequestMethod.POST)
	public String doctorRegister(HttpServletRequest httpServletRequest) {
		
		Doctor doctor = new Doctor();
		String attempt_name = httpServletRequest.getParameter("doctor_name");
		boolean username_available = doctorService.register_check(attempt_name);
		if(username_available) {
		
		doctor.setDoctor_name(httpServletRequest.getParameter("doctor_name"));
		doctor.setDoctor_password(httpServletRequest.getParameter("doctor_password"));
		doctor.setDoctor_gender(httpServletRequest.getParameter("doctor_gender"));
		doctor.setDoctor_age(Integer.parseInt(httpServletRequest.getParameter("doctor_age")));
		doctor.setDoctor_email(httpServletRequest.getParameter("doctor_email"));
		doctor.setDepartment(httpServletRequest.getParameter("department"));
		
		doctorService.register(doctor);
		
		return "redirect:/doctor/accessDoctorLogin";
		}
		else {
			return "register_error";
		}
		
	}
	
	@RequestMapping(value="doctorInfoPage")
	public String DoctorInfo(Model m) {
		m.addAttribute("profile_list", doctorService.getDoctorByUsername(logged_doctor));
		return "occupy/doctor_dashboard";
	}
	
	@RequestMapping(value="/AppointmentList")
	public String AppointmentList(Model m) {
		m.addAttribute("appointment_list", appointmentService.getAppointmentsByDoctorName(logged_doctor));
		return "occupy/doctor_appointment";
	}
	
	@RequestMapping(value="/patientList")
	public String PatientList(Model m) {
		m.addAttribute("patient_list", adminService.getUserList());
		return "occupy/doctor_view_patients";
	}
	
	@RequestMapping(value="/prescriptionList")
	public String PrecriptionList(Model m) {
		m.addAttribute("prescription_list", prescriptionService.getPrescriptionByDoctorName(logged_doctor));
		return "occupy/doctor_prescription";
	}
	
	@RequestMapping(value="/edit_doctor_profile/{id}",method=RequestMethod.GET)
	public String editAppointment(@PathVariable("id")int id , Model uiModel) {
		
		Doctor doctor=  doctorService.getDoctorById(id);
		uiModel.addAttribute("doctor",doctor);
		return "occupy/doctor_edit_profile";
	}
	
	@RequestMapping(value="/edit_doctor_profile/**",method=RequestMethod.POST)
	public String editAppointment(@Valid Doctor doctor) {
		doctorService.updateDoctorProfile(doctor);
		return "redirect:../doctorInfoPage";
	}
	@RequestMapping(value="/logout")
	public String Logout(HttpSession session) {
		session.invalidate();
		return "occupy/logout";
	}
	
	
}
