package au.usyd.clinic.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import au.usyd.clinic.domain.Appointment;
import au.usyd.clinic.domain.User;
import au.usyd.clinic.service.AdminService;
import au.usyd.clinic.service.AppointmentService;
import au.usyd.clinic.service.DoctorService;
import au.usyd.clinic.service.UserService;
import au.usyd.clinic.service.PrescriptionService;

@Controller
@RequestMapping(value="/user")
public class UserController{
	
	public static String logged_user="";
	
	
	@Autowired
	DoctorService doctorService;
	
	@Autowired
	UserService userService;
	@Autowired
	AdminService adminService;
	@Autowired
	PrescriptionService prescriptionService;
	@Autowired
	AppointmentService appointmentService;
	
	@RequestMapping(value="/accessloginpage")
	public String loginPage() {
		return "occupy/user_login";
	}
	
	@RequestMapping(value="/login")
	public String login(HttpServletRequest request) {
		

		String username = request.getParameter("username");
        String password = request.getParameter("password");
		
		
        //User user = userService.getUserById(id);
        User user = userService.getUserByUsername(username);
        HttpSession session = request.getSession(true);
        session.setAttribute("user", user);
        
        if(user!=null) {
        	if(user.getPassword().equals(password)&&user.getUsername().equals(username)) {
        		logged_user=username;
        		return "redirect:UserInfoPage";
        	}else {
        		return "error";
        	}
        	
        	
        }else {
        	return "error";
        }
	
		
	}
	
	
	@RequestMapping(value="/register")
	public String registerPage() {
		return "occupy/user_signup";
	}
	
	@RequestMapping(value="/registerDo",method=RequestMethod.POST)
	public String register(HttpServletRequest httpServletRequest) {
		User user = new User();
		String attempt_name = httpServletRequest.getParameter("username");
		boolean username_available = userService.register_check(attempt_name);
		if(username_available) {
		user.setUsername(httpServletRequest.getParameter("username"));
		user.setPassword(httpServletRequest.getParameter("password"));
		user.setGender(httpServletRequest.getParameter("gender"));
		user.setAge(Integer.parseInt(httpServletRequest.getParameter("age")));
		user.setEmail(httpServletRequest.getParameter("email"));
		
		userService.register(user);
		
		/*int id = 0;
		id = userService.selectUserId(user);
		HttpServletRequest request = null;
		request.getSession().setAttribute("userName", user.getUsername());
		request.getSession().setAttribute("newId", id);
		*/
		return "redirect:/user/accessloginpage";
		}else {
			return "register_error";
		}
	}
	
	@RequestMapping(value="/UserInfoPage")
	public String userInfo(Model m) {
		m.addAttribute("profile_list", userService.getUserByUsername(logged_user));
		return "occupy/user_dashboard";
	}
	
	
	@RequestMapping(value="/AppointmentList")
	public String AppointmentList(Model m) {
		m.addAttribute("appointment_list", appointmentService.getAppointmentsByPatientName(logged_user));
		return "occupy/user_appointment";
	}
	
	@RequestMapping(value="/DoctorList")
	public String PatientList(Model m) {
		m.addAttribute("doctor_list", doctorService.getAllDoctors());
		return "occupy/user_view_doctor";
	}
	
	@RequestMapping(value="/PrescriptionList")
	public String PrecriptionList(Model m) {
		m.addAttribute("prescription_list", prescriptionService.getPrescriptionByPatientName(logged_user));
		return "occupy/user_prescription";
	}
	
	@RequestMapping(value="/logout")
	public String Logout(HttpSession session) {
		session.invalidate();
		return "occupy/logout";
	}
	
	@RequestMapping(value="/edit_user_profile/{id}",method=RequestMethod.GET)
	public String editAppointment(@PathVariable("id")int id , Model uiModel) {
		
		User user=  userService.getUserById(id);
		uiModel.addAttribute("user",user);
		return "occupy/user_edit_profile";
	}
	
	@RequestMapping(value="/edit_user_profile/**",method=RequestMethod.POST)
	public String editAppointment(@Valid User user) {
		userService.updateUserProfile(user);
		return "redirect:../UserInfoPage";
	}
	
	@RequestMapping(value="search_user",method=RequestMethod.POST)
	public String SearchUser(HttpServletRequest httpServletRequest) {
		String search_name = httpServletRequest.getParameter("username");
		return "redirect:../UserInfoPage";
	}
	
	/*@RequestMapping(value="/patientList")
	public String PatientList(Model m) {
		m.addAttribute("patient_list", userService.getAllUsers());
		return "view_patients";
	}*/
	
}
