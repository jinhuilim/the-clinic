package au.usyd.clinic.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import au.usyd.clinic.domain.Admin;
import au.usyd.clinic.domain.Appointment;
import au.usyd.clinic.domain.Prescription;
import au.usyd.clinic.service.AdminService;
import au.usyd.clinic.service.AppointmentService;
import au.usyd.clinic.service.DoctorService;
import au.usyd.clinic.service.PrescriptionService;
import au.usyd.clinic.service.ReviewService;
import au.usyd.clinic.service.UserService;



@Controller
@RequestMapping(value="/admin")
public class AdminController {
	public static String logged_user="";
	
	@Autowired
	AdminService adminService;
	@Autowired
	UserService userService;
	@Autowired
	DoctorService doctorService;
	@Autowired
	PrescriptionService prescriptionService;
	@Autowired
	AppointmentService appointmentService;
	@Autowired
	ReviewService reviewService;
	
	@RequestMapping(value="/accessadminpage")
	public String loginPage(HttpServletRequest request) {
		return "admin_login";
	}
	
	@RequestMapping(value="/adminlogin")
	public String login(HttpServletRequest request) {
		
		int id = Integer.parseInt(request.getParameter("id"));
		String username = request.getParameter("username");
        String password = request.getParameter("password");
		
		
        Admin admin = adminService.getAdminById(id);
        
        if(admin!=null) {
        	if(admin.getAdmin_password().equals(password)&&admin.getAdmin_name().equals(username)) {
        		logged_user=username;
        		return "admin_home";
        	}else {
        		return "error";
        	}
        	
        	
        }else {
        	return "error";
        }
	
	}
	
	
	@RequestMapping(value="/patientlist")
	public String PatientList(Model m) {
		m.addAttribute("patient_list", adminService.getUserList());
		return "admin_view_patients";
	}
	
	@RequestMapping(value="/doctorlist")
	public String DoctorList(Model m) {
		m.addAttribute("doctor_list", adminService.getDoctorList());
		return "admin_view_doctors";
	}
	
	@RequestMapping(value="/appointmentlist")
	public String AppointmentList(Model m) {
		m.addAttribute("appointment_list", adminService.getAllAppointments());
		return "admin_view_appointments";
	}
	
	@RequestMapping(value="/reviewlist")
	public String ReviewList(Model m) {
		m.addAttribute("review_list", reviewService.getAllReview());
		return "admin_view_review";
	}

	@RequestMapping(value="/delete_review/{id}",method=RequestMethod.GET)
	public String ReviewList(@PathVariable("id") int id) {
		reviewService.delete(id);
		// return "redirect:/clinic/admin/AppointmentList";
		return "redirect:../reviewlist";
	}
	
	@RequestMapping(value="/delete_user/{id}",method=RequestMethod.GET)
	public String DeleteUser(@PathVariable("id")int id) {
		adminService.delete_user(id);
		// return "redirect:/clinic/admin/AppointmentList";
		return "redirect:../patientlist";
	}
	
	@RequestMapping(value="/delete_doctor/{id}",method=RequestMethod.GET)
	public String DeleteDoctor(@PathVariable("id")int id) {
		adminService.delete_doctor(id);
		// return "redirect:/clinic/admin/AppointmentList";
		return "redirect:../doctorlist";
	}
	
	@RequestMapping(value="/delete_appointment/{id}",method=RequestMethod.GET)
	public String AppointmentList(@PathVariable("id")int id) {
		adminService.delete_appointment(id);
		// return "redirect:/clinic/admin/AppointmentList";
		return "redirect:../appointmentlist";
	}
	
	@RequestMapping(value="/edit_appointment/{id}",method=RequestMethod.GET)
	public String editAppointment(@PathVariable("id")int id , Model uiModel) {
		
		Appointment appointment=  appointmentService.getAptById(id);
		uiModel.addAttribute("appointment",appointment);
		return "admin_edit_appointment";
	}
	
	@RequestMapping(value="/edit_appointment/**",method=RequestMethod.POST)
	public String editAppointment(@Valid Appointment appointment) {
		appointmentService.updateAppointment(appointment);
		return "redirect:../appointmentlist";
	}
	
	@RequestMapping(value="/prescriptionlist")
	public String PrecriptionList(Model m) {
		m.addAttribute("prescription_list", prescriptionService.getAllPrescription());
		return "admin_view_prescription";
	}
	
	@RequestMapping(value="/delete_prescription/{id}",method=RequestMethod.GET)
	public String DeletePrescription(@PathVariable("id")int id) {
		adminService.delete_prescription(id);
		// return "redirect:/clinic/admin/AppointmentList";
		return "redirect:../prescriptionlist";
	}

	@RequestMapping(value="/edit_prescription/{id}",method=RequestMethod.GET)
	public String editPrescription(@PathVariable("id")int id , Model uiModel) {
		
		Prescription prescription=  prescriptionService.getPresById(id);
		uiModel.addAttribute("prescription",prescription);
		return "admin_edit_prescription";
	}
	
	@RequestMapping(value="/edit_prescription/**",method=RequestMethod.POST)
	public String editPrescription(@Valid Prescription prescription) {
		//can't pass the value of medicine_name from the jsp to here.(Always null) but edit_appointment works fine. So weird.
		System.out.println(prescription.getMedicine_name());
		prescriptionService.updatePrescription(prescription);
		return "redirect:../prescriptionlist";
	}
	
	@RequestMapping(value="/search_user",method=RequestMethod.POST)
	public String SearchUser(Model user, Model user_prescription, Model user_appointment, Model user_review, HttpServletRequest httpServletRequest) {
		String search_name = httpServletRequest.getParameter("username");
		user.addAttribute("user_list", userService.getUserByUsername(search_name));
		user_prescription.addAttribute("user_prescription", prescriptionService.getPrescriptionByPatientName(search_name));
		user_appointment.addAttribute("user_appointment", appointmentService.getAppointmentsByPatientName(search_name));
		user_review.addAttribute("user_review", reviewService.getReviewByUsername(search_name));
		return "admin_check_user";
	}
	
	@RequestMapping(value="/search_doctor",method=RequestMethod.POST)
	public String SearchDoctor(Model doctor,Model doctor_prescription, Model doctor_appointment, Model doctor_review, HttpServletRequest httpServletRequest) {
		String search_name = httpServletRequest.getParameter("username");
		doctor.addAttribute("doctor_list", doctorService.getDoctorByUsername(search_name));
		doctor_prescription.addAttribute("doctor_prescription", prescriptionService.getPrescriptionByDoctorName(search_name));
		doctor_appointment.addAttribute("doctor_appointment", appointmentService.getAppointmentsByDoctorName(search_name));
		doctor_review.addAttribute("doctor_review", reviewService.getReviewByUsername(search_name));
		return "admin_check_doctor";
	}
	


	

	
	
	
}

