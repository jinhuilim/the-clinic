package au.usyd.clinic.dao;

import java.util.List;

import au.usyd.clinic.domain.Review;

public interface ReviewDao {
	
	public void addReview(Review review);
	
	public List<Review> getAllReview();

	public List<Review> getReviewByUsername(String username);
	
	public void delete(int id);
}
