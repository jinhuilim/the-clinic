package au.usyd.clinic.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.classic.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import au.usyd.clinic.domain.Appointment;
import au.usyd.clinic.domain.Doctor;
import au.usyd.clinic.domain.Prescription;

@Repository("appointmentDao")
public class AppointmentDaoImpl implements AppointmentDao {
	
	
	@Autowired
	private SessionFactory sessionFactory;
	
	
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}
	@Autowired
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	
	
	
	@Override
	public void addAppointment(Appointment apt) {
		// TODO Auto-generated method stub
		this.sessionFactory.getCurrentSession().save(apt);
	}

	@Override
	public Appointment getAptById(int id) {
		// TODO Auto-generated method stub
		Session currentSession = this.sessionFactory.getCurrentSession();
		Appointment apt = (Appointment) currentSession.get(Appointment.class, id);
		
		return apt;
	}

	@Override
	public List<Appointment> getAllAppointments() {
		// TODO Auto-generated method stub
		
		List<Appointment> list = sessionFactory.getCurrentSession().createQuery("FROM Appointment").list();
		
		return list;
	}
	@Override
	public void delete(int id) {
		// TODO Auto-generated method stub
		Session currentSession = this.sessionFactory.getCurrentSession();
		Appointment apt = (Appointment) currentSession.get(Appointment.class, id);
		currentSession.delete(apt);
	}
	@Override
	public List<Appointment> getAppointmentsByDoctorName(String username) {
		// TODO Auto-generated method stub
		Session currentSession = this.sessionFactory.getCurrentSession();
		Query query = sessionFactory.getCurrentSession().createQuery("from Appointment p where p.doctor_name = :username");
		query.setParameter("username", username);
		List<Appointment> list = query.list();
		return list;
	}
	
	@Override
	public List<Appointment> getAppointmentsByPatientName(String username) {
		// TODO Auto-generated method stub
		Session currentSession = this.sessionFactory.getCurrentSession();
		Query query = sessionFactory.getCurrentSession().createQuery("from Appointment p where p.patient_name = :username");
		query.setParameter("username", username);
		List<Appointment> list = query.list();
		return list;
	}
	
	@Override
	public void updateAppointment(Appointment appointment) {
		// TODO Auto-generated method stub
		Session currentSession = this.sessionFactory.getCurrentSession();

		currentSession.merge(appointment);

	}

	
	

}
