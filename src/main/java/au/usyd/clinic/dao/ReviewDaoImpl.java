package au.usyd.clinic.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.classic.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import au.usyd.clinic.domain.Appointment;
import au.usyd.clinic.domain.Prescription;
import au.usyd.clinic.domain.Review;
import au.usyd.clinic.domain.User;

@Repository("reviewDao")
public class ReviewDaoImpl implements ReviewDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}
	@Autowired
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	@Override
	public void addReview(Review review) {
		// TODO Auto-generated method stub
		this.sessionFactory.getCurrentSession().save(review);
	}

	@Override
	public List<Review> getAllReview() {
		// TODO Auto-generated method stub
		List<Review> list = sessionFactory.getCurrentSession().createQuery("FROM Review").list();
		return list;
	}
	
	@Override
	public List<Review> getReviewByUsername(String username) {
		// TODO Auto-generated method stub
		Query query = this.sessionFactory.getCurrentSession().createQuery("from Review p where p.name = :name");
		 query.setString("name", username);
		 List<Review> list = query.list();
		 return list;

	}
	
	@Override
	public void delete(int id) {
		// TODO Auto-generated method stub
		Session currentSession = this.sessionFactory.getCurrentSession();
		Review review = (Review) currentSession.get(Review.class, id);
		currentSession.delete(review);
	}
}
