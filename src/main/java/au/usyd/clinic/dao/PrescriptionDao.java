package au.usyd.clinic.dao;

import java.util.List;

import au.usyd.clinic.domain.Appointment;
import au.usyd.clinic.domain.Prescription;

public interface PrescriptionDao {
	public void addPrescription(Prescription pres);
	
	public Prescription getPresById(int id);
	
	public List<Prescription> getAllPrescription();
	
	public void delete(int id);
	
	public void updatePrescription(Prescription prescription);
	
	public List<Prescription> getPrescriptionByDoctorName(String username);
	
	public List<Prescription> getPrescriptionByPatientName(String username);
}
