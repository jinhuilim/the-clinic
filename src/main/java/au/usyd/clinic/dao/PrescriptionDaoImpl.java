package au.usyd.clinic.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import au.usyd.clinic.domain.Appointment;
import au.usyd.clinic.domain.Prescription;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.classic.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;



@Repository("prescriptionDao")
public class PrescriptionDaoImpl implements PrescriptionDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}
	@Autowired
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	
	
	
	@Override
	public void addPrescription(Prescription pres) {
		// TODO Auto-generated method stub
		this.sessionFactory.getCurrentSession().save(pres);
	}

	@Override
	public Prescription getPresById(int id) {
		// TODO Auto-generated method stub
		Session currentSession = this.sessionFactory.getCurrentSession();
		Prescription pres = (Prescription) currentSession.get(Prescription.class, id);
		
		return pres;
	}

	@Override
	public List<Prescription> getAllPrescription() {
		// TODO Auto-generated method stub
		
		List<Prescription> list = sessionFactory.getCurrentSession().createQuery("FROM Prescription").list();
		
		return list;
	}
	@Override
	public void delete(int id) {
		// TODO Auto-generated method stub
		Session currentSession = this.sessionFactory.getCurrentSession();
		Prescription pres = (Prescription) currentSession.get(Prescription.class, id);
		currentSession.delete(pres);
	}
	
	@Override
	public void updatePrescription(Prescription prescription) {
		// TODO Auto-generated method stub
		Session currentSession = this.sessionFactory.getCurrentSession();

		currentSession.merge(prescription);

	}
	@Override
	public List<Prescription> getPrescriptionByDoctorName(String username) {
		Session currentSession = this.sessionFactory.getCurrentSession();
		Query query = sessionFactory.getCurrentSession().createQuery("from Prescription p where p.doctor_name = :username");
		query.setParameter("username", username);
		List<Prescription> list = query.list();
		return list;
	}
	@Override
	public List<Prescription> getPrescriptionByPatientName(String username) {
		// TODO Auto-generated method stub
		Session currentSession = this.sessionFactory.getCurrentSession();
		Query query = sessionFactory.getCurrentSession().createQuery("from Prescription p where p.patient_name = :username");
		query.setParameter("username", username);
		List<Prescription> list = query.list();
		return list;
	}

	

}
