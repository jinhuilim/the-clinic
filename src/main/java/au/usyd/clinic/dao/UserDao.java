package au.usyd.clinic.dao;

import java.util.List;

import au.usyd.clinic.domain.User;

public interface UserDao {
	
	public User getUserById(int id);
	
	public void register(User user);
	
	public User getUserByUsername(String username);
	//public int selectUserId(String hql);
	
	public List<User> getAllUsers();
	
	public void updateUserProfile(User user);

	public void delete(int id);
	
	public boolean register_check(String username);
}
