package au.usyd.clinic.dao;

import au.usyd.clinic.domain.Admin;

public interface AdminDao {
	
	public Admin getAdminById(int id);
	

}
