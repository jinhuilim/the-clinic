package au.usyd.clinic.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.classic.Session;
import org.hibernate.criterion.Example;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import au.usyd.clinic.domain.Appointment;
import au.usyd.clinic.domain.Doctor;
import au.usyd.clinic.domain.User;


@Repository("userDao")
public class UserDaoImpl implements UserDao {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	
	
	
	
	
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}
	@Autowired
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	
	
	
	@Override
	public User getUserById(int id) {
		// TODO Auto-generated method stub
		Session currentSession = this.sessionFactory.getCurrentSession();
		User user = (User) currentSession.get(User.class, id);
		
		
		
		return user;
	}

	@Override
	public void register(User user) {
		// TODO Auto-generated method stub
		this.sessionFactory.getCurrentSession().save(user);
		
		
		
	}
	@Override
	public User getUserByUsername(String username) {
		// TODO Auto-generated method stub
		Query query = this.sessionFactory.getCurrentSession().createQuery("from User p where p.username = :username");
		 query.setString("username", username);
		 User user= (User) query.uniqueResult();
		 System.out.println(user.getUsername());

		return user;
	}
	@Override
	public List<User> getAllUsers() {
		// TODO Auto-generated method stub
		
		List<User> list = sessionFactory.getCurrentSession().createQuery("FROM User").list();
		
		return list;
	}
	@Override
	public void updateUserProfile(User user) {
		// TODO Auto-generated method stub
		Session currentSession = this.sessionFactory.getCurrentSession();

		currentSession.merge(user);
		
	}
	
	@Override
	public void delete(int id) {
		// TODO Auto-generated method stub
		Session currentSession = this.sessionFactory.getCurrentSession();
		User user = (User) currentSession.get(User.class, id);
		currentSession.delete(user);
	}
	
	
	/*@Override
	public int selectUserId(String hql) {
		// TODO Auto-generated method stub
		Session currentSession = this.sessionFactory.getCurrentSession();

		Query query = currentSession.createQuery(hql);
		
		return (Integer) query.uniqueResult();
	}*/

	@Override
	public boolean register_check(String username) {
		// TODO Auto-generated method stub
		List<User> list = sessionFactory.getCurrentSession().createQuery("FROM User").list();
		for (int i=0; i<list.size(); i++)
		{ 
			String name = list.get(i).getUsername();
			if (name.equals(username)) {
				return false;
			}
		}
		return true;
	}
	

}
