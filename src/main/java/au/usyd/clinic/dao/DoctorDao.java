package au.usyd.clinic.dao;

import java.util.List;

import au.usyd.clinic.domain.Doctor;
import au.usyd.clinic.domain.User;

public interface DoctorDao {
	
	public Doctor getDoctorById(int id);
	
	public void register(Doctor doctor);
	
	public Doctor getDoctorByUsername(String username);
	
	public List<Doctor> getAllDoctors();
	
	public void updateDoctorProfile(Doctor doctor);

	public void delete(int id);
	
	public boolean register_check(String username);
}
