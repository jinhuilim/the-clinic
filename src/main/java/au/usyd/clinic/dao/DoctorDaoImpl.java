package au.usyd.clinic.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.classic.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import au.usyd.clinic.domain.Doctor;
import au.usyd.clinic.domain.User;


@Repository("doctorDao")
public class DoctorDaoImpl implements DoctorDao {
	
	
	@Autowired
	private SessionFactory sessionFactory;
	

	
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}
	@Autowired
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	public Doctor getDoctorById(int id) {
		// TODO Auto-generated method stub
		
		Session currentSession = this.sessionFactory.getCurrentSession();
		Doctor doctor = (Doctor) currentSession.get(Doctor.class, id);
		
				
		return doctor;
	}

	@Override
	public void register(Doctor doctor) {
		// TODO Auto-generated method stub
		
		this.sessionFactory.getCurrentSession().save(doctor);
		
	}
	
	@Override
	public Doctor getDoctorByUsername(String username) {
		// TODO Auto-generated method stub
		Query query = this.sessionFactory.getCurrentSession().createQuery("from Doctor p where p.doctor_name = :username");
		 query.setString("username", username);
		 Doctor doctor= (Doctor) query.uniqueResult();

		return doctor;
	}
	
	@Override
	public List<Doctor> getAllDoctors() {
		// TODO Auto-generated method stub
		
		List<Doctor> list = sessionFactory.getCurrentSession().createQuery("FROM Doctor").list();
		
		
		return list;
	}
	
	@Override
	public void updateDoctorProfile(Doctor doctor) {
		// TODO Auto-generated method stub
		Session currentSession = this.sessionFactory.getCurrentSession();

		currentSession.merge(doctor);
		
	}
	
	@Override
	public void delete(int id) {
		// TODO Auto-generated method stub
		Session currentSession = this.sessionFactory.getCurrentSession();
		Doctor doctor = (Doctor) currentSession.get(Doctor.class, id);
		currentSession.delete(doctor);
	}
	@Override
	public boolean register_check(String username) {
		// TODO Auto-generated method stub
		List<Doctor> list = sessionFactory.getCurrentSession().createQuery("FROM Doctor").list();
		for (int i=0; i<list.size(); i++)
		{ 
			String name = list.get(i).getDoctor_name();
			if (name.equals(username)) {
				return false;
			}
		}
		return true;
	}
	

}
