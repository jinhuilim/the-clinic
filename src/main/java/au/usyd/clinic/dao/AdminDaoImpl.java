package au.usyd.clinic.dao;

import org.hibernate.SessionFactory;
import org.hibernate.classic.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import au.usyd.clinic.domain.Admin;


@Repository("adminDao")
public class AdminDaoImpl implements AdminDao {
	
	
	@Autowired
	private SessionFactory sessionFactory;
	

	
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}
	@Autowired
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	public Admin getAdminById(int id) {
		// TODO Auto-generated method stub
		
		Session currentSession = this.sessionFactory.getCurrentSession();
		Admin admin = (Admin) currentSession.get(Admin.class, id);
		
				
		return admin;
	}

}

