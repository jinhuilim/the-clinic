package au.usyd.clinic.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Appointment")
public class Appointment {
	
	@Id
	@GeneratedValue
	@Column(name="id")
	private int id;
	
	@Column(name="patient_name")
	private String patient_name;
	
	@Column(name="date")
	private String date;
	
	@Column(name="doctor_name")
	private String doctor_name;

	public Appointment() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Appointment(int id, String patient_name, String date, String doctor_name) {
		super();
		this.id = id;
		this.patient_name = patient_name;
		this.date = date;
		this.doctor_name = doctor_name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPatient_name() {
		return patient_name;
	}

	public void setPatient_name(String patient_name) {
		this.patient_name = patient_name;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getDoctor_name() {
		return doctor_name;
	}

	public void setDoctor_name(String doctor_name) {
		this.doctor_name = doctor_name;
	}

	@Override
	public String toString() {
		return "Appointment [id=" + id + ", patient_name=" + patient_name + ", date=" + date + ", doctor_name="
				+ doctor_name + "]";
	}
	
	
}
