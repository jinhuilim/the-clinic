package au.usyd.clinic.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Doctor")
public class Doctor {
	
	
	
	@Id
	@GeneratedValue
	@Column(name="doctor_id")
	private int doctor_id;
	
	@Column(name="doctor_name")
	private String doctor_name;
	
	@Column(name="doctor_password")
	private String doctor_password;
	
	@Column(name="doctor_gender")
	private String doctor_gender;
	
	@Column(name="doctor_age")
	private int doctor_age;
	
	@Column(name="doctor_email")
	private String doctor_email;
	
	@Column(name="department")
	private String department;

	public Doctor() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Doctor(int doctor_id, String doctor_name, String doctor_password, String doctor_gender, int doctor_age,
			String doctor_email, String department) {
		super();
		this.doctor_id = doctor_id;
		this.doctor_name = doctor_name;
		this.doctor_password = doctor_password;
		this.doctor_gender = doctor_gender;
		this.doctor_age = doctor_age;
		this.doctor_email = doctor_email;
		this.department = department;
	}

	public int getDoctor_id() {
		return doctor_id;
	}

	public void setDoctor_id(int doctor_id) {
		this.doctor_id = doctor_id;
	}

	public String getDoctor_name() {
		return doctor_name;
	}

	public void setDoctor_name(String doctor_name) {
		this.doctor_name = doctor_name;
	}

	public String getDoctor_password() {
		return doctor_password;
	}

	public void setDoctor_password(String doctor_password) {
		this.doctor_password = doctor_password;
	}

	public String getDoctor_gender() {
		return doctor_gender;
	}

	public void setDoctor_gender(String doctor_gender) {
		this.doctor_gender = doctor_gender;
	}

	public int getDoctor_age() {
		return doctor_age;
	}

	public void setDoctor_age(int doctor_age) {
		this.doctor_age = doctor_age;
	}

	public String getDoctor_email() {
		return doctor_email;
	}

	public void setDoctor_email(String doctor_email) {
		this.doctor_email = doctor_email;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	@Override
	public String toString() {
		return "Doctor [doctor_id=" + doctor_id + ", doctor_name=" + doctor_name + ", doctor_password="
				+ doctor_password + ", doctor_gender=" + doctor_gender + ", doctor_age=" + doctor_age
				+ ", doctor_email=" + doctor_email + ", department=" + department + "]";
	}
	
	
}
