package au.usyd.clinic.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Prescription")
public class Prescription {
	
	@Id
	@GeneratedValue
	@Column(name="id")
	private int id;
	
	@Column(name="patient_name")
	private String patient_name;
	
	@Column(name="medicine_name")
	private String medicine_name;
	
	@Column(name="doctor_name")
	private String doctor_name;

	public Prescription() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Prescription(int id, String patient_name, String medicine_name, String doctor_name) {
		super();
		this.id = id;
		this.patient_name = patient_name;
		this.medicine_name = medicine_name;
		this.doctor_name = doctor_name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPatient_name() {
		return patient_name;
	}

	public void setPatient_name(String patient_name) {
		System.out.println(patient_name);
		this.patient_name = patient_name;
	}

	public String getMedicine_name() {
		
		return medicine_name;
	}

<<<<<<< HEAD
	public void setMedince_name(String medicine_name) {
=======
	public void setPrescription(String medicine_name) {
		System.out.println(medicine_name);
>>>>>>> 08c2bf24d7f62923457088886eb270c6ecadbfdf
		this.medicine_name = medicine_name;
	}

	public String getDoctor_name() {
		return doctor_name;
	}

	public void setDoctor_name(String doctor_name) {
		System.out.println(doctor_name);
		this.doctor_name = doctor_name;
	}

	@Override
	public String toString() {
		return "Appointment [id=" + id + ", patient_name=" + patient_name + ", medicine_name=" + medicine_name + ", doctor_name="
				+ doctor_name + "]";
	}


	
}
