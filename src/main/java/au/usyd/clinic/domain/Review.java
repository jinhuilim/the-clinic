package au.usyd.clinic.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Review")
public class Review {
	
	@Id
	@GeneratedValue
	@Column(name="id")
	private int id;
	
	@Column(name="name")
	private String name;
	
	@Column(name="messages")
	private String messages;
	

	public Review() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Review(int id, String name, String messages) {
		super();
		this.id = id;
		this.name = name;
		this.messages = messages;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMessages() {
		
		return messages;
	}

	public void setMessages(String messages) {
		this.messages = messages;
	}


	@Override
	public String toString() {
		return "Review [id=" + id + ", name=" + name + ", messages=" + messages + "]";
	}


	
}
