package au.usyd.clinic.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import au.usyd.clinic.dao.AppointmentDao;
import au.usyd.clinic.domain.Appointment;
import au.usyd.clinic.domain.Prescription;


@Service("appointmentService")
@Transactional
public class AppointmentServiceImpl implements AppointmentService {

	
	@Autowired
	AppointmentDao appointmentDao;
	
	@Override
	public void addAppointment(Appointment apt) {
		// TODO Auto-generated method stub
		appointmentDao.addAppointment(apt);
	}

	@Override
	public Appointment getAptById(int id) {
		// TODO Auto-generated method stub
		Appointment apt = appointmentDao.getAptById(id);
		
		return apt;
	}

	@Override
	public List<Appointment> getAllAppointments() {
		// TODO Auto-generated method stub
		
		List<Appointment> list = appointmentDao.getAllAppointments();
		
		return list;
	}
	
	@Override
	public List<Appointment> getAppointmentsByDoctorName(String username) {
		List<Appointment> list = appointmentDao.getAppointmentsByDoctorName(username);
		
		return list;
	}
	
	@Override
	public List<Appointment> getAppointmentsByPatientName(String username) {
		List<Appointment> list = appointmentDao.getAppointmentsByPatientName(username);
		
		return list;
	}

	@Override
	public void updateAppointment(Appointment appointment) {
		// TODO Auto-generated method stub
		appointmentDao.updateAppointment(appointment);
		
	}


	


}
