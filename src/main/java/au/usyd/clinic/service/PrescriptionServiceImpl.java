package au.usyd.clinic.service;

import java.util.List;

import org.hibernate.SessionFactory;
import org.hibernate.classic.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import au.usyd.clinic.dao.PrescriptionDao;
import au.usyd.clinic.domain.Appointment;
import au.usyd.clinic.domain.Prescription;


@Service("PrescriptionService")
@Transactional
public class PrescriptionServiceImpl implements PrescriptionService {

	@Autowired
	private SessionFactory sessionFactory;
	
	
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}
	@Autowired
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	@Autowired
	PrescriptionDao prescriptionDao;

	@Override
	public void addPrescription(Prescription pres) {
		// TODO Auto-generated method stub
		prescriptionDao.addPrescription(pres);
	}

	@Override
	public List<Prescription> getAllPrescription() {
		// TODO Auto-generated method stub
		List<Prescription> list = prescriptionDao.getAllPrescription();
		
		return list;
	}

	@Override
	public Prescription getPresById(int id) {
		// TODO Auto-generated method stub
		Session currentSession = this.sessionFactory.getCurrentSession();
		Prescription pres = (Prescription) currentSession.get(Prescription.class, id);
		
		return pres;
	}
	@Override
	public void updatePrescription(Prescription prescription) {
		// TODO Auto-generated method stub
		prescriptionDao.updatePrescription(prescription);
	}
	@Override
	public List<Prescription> getPrescriptionByDoctorName(String username) {
		List<Prescription> list = prescriptionDao.getPrescriptionByDoctorName(username);
		
		return list;
	}
	@Override
	public List<Prescription> getPrescriptionByPatientName(String username) {
		// TODO Auto-generated method stub
		List<Prescription> list = prescriptionDao.getPrescriptionByPatientName(username);
		
		return list;
	}
	
	
}
