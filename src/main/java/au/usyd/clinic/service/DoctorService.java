package au.usyd.clinic.service;

import java.util.List;

import au.usyd.clinic.domain.Doctor;
import au.usyd.clinic.domain.Prescription;
import au.usyd.clinic.domain.User;

public interface DoctorService {
		
	public Doctor getDoctorByUsername(String username);
	
	public Doctor getDoctorById(int id);
	
	public void register(Doctor doctor);
	
	public List<Doctor> getAllDoctors();
	
	public List<Prescription> getAllPrescription();
	
	public void updateDoctorProfile(Doctor doctor);
	
	public boolean register_check(String username);
	
	
}
