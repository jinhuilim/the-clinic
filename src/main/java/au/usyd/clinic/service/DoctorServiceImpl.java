package au.usyd.clinic.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import au.usyd.clinic.dao.DoctorDao;
import au.usyd.clinic.domain.Doctor;
import au.usyd.clinic.domain.Prescription;
import au.usyd.clinic.domain.User;
import au.usyd.clinic.dao.PrescriptionDao;

@Service("doctorService")
@Transactional
public class DoctorServiceImpl implements DoctorService {
	
	@Autowired
	DoctorDao doctorDao;
	
	@Autowired
	PrescriptionDao prescriptionDao;
	
	@Override
	public Doctor getDoctorByUsername(String username) {
		// TODO Auto-generated method stub
		Doctor doctor = doctorDao.getDoctorByUsername(username);
		
		return doctor;
	}

	@Override
	public void register(Doctor doctor) {
		// TODO Auto-generated method stub
		
		doctorDao.register(doctor);
	}

	@Override
	public List<Doctor> getAllDoctors() {
		// TODO Auto-generated method stub
		List<Doctor> list = doctorDao.getAllDoctors();
		
		return list;
	}
	
	@Override
	public List<Prescription> getAllPrescription() {
		// TODO Auto-generated method stub
		List<Prescription> list = prescriptionDao.getAllPrescription();
		
		return list;
	}
	
	public static void main(String[] args) {
		DoctorService ds = new DoctorServiceImpl();
		List<Doctor> list = ds.getAllDoctors();
		System.out.println(list);
	}

	@Override
	public Doctor getDoctorById(int id) {
		// TODO Auto-generated method stub
		Doctor doctor =  doctorDao.getDoctorById(id);
		
		return doctor;
	}

	@Override
	public void updateDoctorProfile(Doctor doctor) {
		// TODO Auto-generated method stub
		doctorDao.updateDoctorProfile(doctor);
	}

	@Override
	public boolean register_check(String username) {
		// TODO Auto-generated method stub
		return doctorDao.register_check(username);
	}

}
