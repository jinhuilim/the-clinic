package au.usyd.clinic.service;

import java.util.List;

import au.usyd.clinic.domain.Appointment;
import au.usyd.clinic.domain.User;

public interface UserService {

	public User getUserById(int id);
	
	public void register(User user);
	
	public User getUserByUsername(String username);
	
	public List<User> getAllUsers();
	//public int selectUserId(User user);
	public void updateUserProfile(User user);
	
	public boolean register_check(String username);
}
