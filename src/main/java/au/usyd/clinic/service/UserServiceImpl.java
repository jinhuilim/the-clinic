package au.usyd.clinic.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import au.usyd.clinic.dao.UserDao;
import au.usyd.clinic.dao.UserDaoImpl;
import au.usyd.clinic.domain.User;


@Service("userService")
@Transactional
public class UserServiceImpl implements UserService {
	
	@Autowired
	UserDao userDao;

	

	@Override
	public User getUserById(int id) {
		// TODO Auto-generated method stub
		
		User user =  userDao.getUserById(id);
		
		return user;
	}



	@Override
	public void register(User user) {
		// TODO Auto-generated method stub
		userDao.register(user);
		
	}



	@Override
	public User getUserByUsername(String username) {
		// TODO Auto-generated method stub
		User user = userDao.getUserByUsername(username);
		
		return user;
	}



	/*@Override
	public int selectUserId(User user) {
		// TODO Auto-generated method stub
		int id=0;
		
		String hql="select id from PatientUser where username='"+user.getUsername()+"' and password='"+user.getPassword()+"'";
		
		id = userDao.selectUserId(hql);
		return id;
	}
	
	*/
	public static void main(String[] args) {
		UserDao userDao = new UserDaoImpl();
		List<User> list = userDao.getAllUsers();
		System.out.println(list);
	}



	@Override
	public List<User> getAllUsers() {
		// TODO Auto-generated method stub
		List<User> list = userDao.getAllUsers();
		
		return list;
	}



	@Override
	public void updateUserProfile(User user) {
		// TODO Auto-generated method stub
		userDao.updateUserProfile(user);
	}
	
	@Override
	public boolean register_check(String username) {
		// TODO Auto-generated method stub
		return userDao.register_check(username);
	}


}
