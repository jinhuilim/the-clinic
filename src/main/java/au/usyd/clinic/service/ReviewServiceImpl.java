package au.usyd.clinic.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import au.usyd.clinic.dao.ReviewDao;
import au.usyd.clinic.domain.Prescription;
import au.usyd.clinic.domain.Review;


@Service("reviewService")
@Transactional
public class ReviewServiceImpl implements ReviewService {

	
	@Autowired
	ReviewDao reviewDao;
	
	@Override
	public void addReview(Review review) {
		// TODO Auto-generated method stub
		reviewDao.addReview(review);
	}


	@Override
	public List<Review> getAllReview() {
		// TODO Auto-generated method stub
		
		List<Review> list = reviewDao.getAllReview();
		
		return list;
	}


	@Override
	public List<Review> getReviewByUsername(String username) {
		// TODO Auto-generated method stub
		List<Review> list = reviewDao.getReviewByUsername(username);
		
		return list;
	}


	@Override
	public void delete(int id) {
		// TODO Auto-generated method stub
		reviewDao.delete(id);
	}
	



	


}