package au.usyd.clinic.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import au.usyd.clinic.dao.AdminDao;
import au.usyd.clinic.dao.AppointmentDao;
import au.usyd.clinic.dao.DoctorDao;
import au.usyd.clinic.dao.PrescriptionDao;
import au.usyd.clinic.dao.UserDao;
import au.usyd.clinic.dao.UserDaoImpl;
import au.usyd.clinic.domain.Admin;
import au.usyd.clinic.domain.Appointment;
import au.usyd.clinic.domain.Doctor;
import au.usyd.clinic.domain.User;

@Service("adminService")
@Transactional
public class AdminServiceImpl implements AdminService {
	
	@Autowired
	AdminDao adminDao;
	
	@Autowired
	UserDao userDao;
	@Autowired
	DoctorDao doctorDao;
	@Autowired
	AppointmentDao appointmentDao;
	@Autowired
	PrescriptionDao prescriptionDao;
	
	@Override
	public List<User> getUserList() {
		// TODO Auto-generated method stub
		
		List<User> list = userDao.getAllUsers();
		
		return list;
 
		
	}
	
	@Override
	public List<Doctor> getDoctorList() {
		// TODO Auto-generated method stub
		List<Doctor> list = doctorDao.getAllDoctors();
		
		return list;
		
	}
	
	
	@Override
	public Admin getAdminById(int id) {
		// TODO Auto-generated method stub
		Admin admin= adminDao.getAdminById(id);
		
		return admin;
	}

	@Override
	public List<Appointment> getAllAppointments() {
		// TODO Auto-generated method stub
		return appointmentDao.getAllAppointments();
	}

	@Override
	public void delete_appointment(int id) {
		// TODO Auto-generated method stub
		appointmentDao.delete(id);
		
	}
	
	@Override
	public void delete_prescription(int id) {
		// TODO Auto-generated method stub
		prescriptionDao.delete(id);
		
	}

	@Override
	public void delete_user(int id) {
		// TODO Auto-generated method stub
		userDao.delete(id);
	}

	@Override
	public void delete_doctor(int id) {
		// TODO Auto-generated method stub
		doctorDao.delete(id);
	}
	
	

}
