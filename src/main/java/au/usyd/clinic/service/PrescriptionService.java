package au.usyd.clinic.service;

import java.util.List;

import au.usyd.clinic.domain.Appointment;
import au.usyd.clinic.domain.Prescription;

public interface PrescriptionService {
	
	public void addPrescription(Prescription apt);
	
	public List<Prescription> getAllPrescription();
	
	public Prescription getPresById(int id);
	
	public void updatePrescription(Prescription prescription);
	
	public List<Prescription> getPrescriptionByDoctorName(String username);
	
	public List<Prescription> getPrescriptionByPatientName(String username);
	

}
