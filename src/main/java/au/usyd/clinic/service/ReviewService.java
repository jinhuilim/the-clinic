package au.usyd.clinic.service;

import java.util.List;

import au.usyd.clinic.domain.Review;
import au.usyd.clinic.domain.Prescription;

public interface ReviewService {
	
	public void addReview(Review review);
	
	public List<Review> getAllReview();
	
	public List<Review> getReviewByUsername(String username);
	
	public void delete(int id);
	
}
