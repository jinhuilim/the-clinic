package au.usyd.clinic.service;

import java.util.List;

import au.usyd.clinic.domain.Admin;
import au.usyd.clinic.domain.Doctor;
import au.usyd.clinic.domain.User;
import au.usyd.clinic.domain.Appointment;


public interface AdminService {

	

	public Admin getAdminById(int id);

	public List<User> getUserList();
	
	public List<Doctor> getDoctorList();
	
	public List<Appointment> getAllAppointments();
	
	public void delete_appointment(int id);
	
	public void delete_prescription(int id);
	
	public void delete_user(int id);
	
	public void delete_doctor(int id);
	
	

	
	//public int selectUserId(User user);
}