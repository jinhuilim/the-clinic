package au.usyd.clinic.service;

import java.util.List;

import au.usyd.clinic.domain.Appointment;
import au.usyd.clinic.domain.Prescription;

public interface AppointmentService {
	
	public void addAppointment(Appointment apt);
	
	public Appointment getAptById(int id);
	
	public List<Appointment> getAllAppointments();
	
	public List<Appointment> getAppointmentsByDoctorName(String username);
	
	public List<Appointment> getAppointmentsByPatientName(String username);
	
	public void updateAppointment(Appointment appointment);
}
